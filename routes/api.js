const express = require('express')
const router = express.Router()
const Recipe = require('../models/recipes')
const moment = require('moment')

router.get('/recipes', (req, res, next) => {
  Recipe.find({})
    .then((recipes) => {
      res.send(recipes)
    })
    .catch(next)
})

router.post('/recipes', (req, res, next) => {
  Recipe.create(req.body)
    .then((recipe) => {
      res.send(recipe)
    })
    .catch(next)
})

router.put('/receipe/:id', function (req, res, next) {
  Recipe.findOneAndUpdate({ _id: req.params.id }, req.body).then((receipe) => {
    Recipe.findOne({ _id: req.params.id }).then((receipe) => {
      res.send(receipe)
    })
  })
})

router.delete('/recipe/:id', (req, res, next) => {
  Recipe.findOneAndDelete({
    _id: req.params.id,
  }).then((recipe) => {
    res.send(recipe)
  })
})

module.exports = router

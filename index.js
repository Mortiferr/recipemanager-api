const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const bodyParser = require('body-parser')
const swaggerJsdoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')

const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'ReceipeManager API',
      version: '0.1.0',
      description:
        'This is a simple CRUD API application made with Express and documented with Swagger',
      license: {
        name: 'MIT',
        url: 'https://spdx.org/licenses/MIT.html',
      },
    },
    servers: [
      {
        url: 'http://localhost:3001/api',
      },
    ],
  },
  apis: ['./routes/api.js'],
}

const specs = swaggerJsdoc(options)

// set up our express app
const app = express()
app.use(
  '/api-docs',
  swaggerUi.serve,
  swaggerUi.setup(specs, { explorer: true })
)

const corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200,
}

app.use(cors(corsOptions))

// connect to mongodb
mongoose.connect('mongodb://localhost/recipemanager')
mongoose.Promise = global.Promise

app.use(express.static('public'))

app.use(express.json())
// initialize routes
app.use('/api', require('./routes/api'))

// error handling middleware
app.use(function (err, req, res, next) {
  res.status(422).send({ error: err.message })
})
// listen for requests
app.listen(process.env.port || 3001, function () {
  console.log('ready')
})

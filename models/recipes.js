const mongoose = require('mongoose')
const Schema = mongoose.Schema

const RecipeSchema = new Schema(
  {
    title: {
      type: String,
      required: [true, 'A title is required.'],
    },
    description: {
      type: String,
    },
    totalTime: {
      type: String,
    },
  },
  { timestamps: true }
)

const Recipe = mongoose.model('recipe', RecipeSchema)

module.exports = Recipe
